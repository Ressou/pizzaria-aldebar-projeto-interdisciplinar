﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cardapio.aspx.cs" Inherits="cardapio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="icon" type="image/png" href="_img/icon.png" />
   
    <!-- Links -->
    <link rel="stylesheet" href="_css/global.css">
    <link rel="stylesheet" href="_fonts/fa/css/all.min.css">

    <!-- metas -->

    <title> Pizzaria Aldebarã | Cardapio </title>

</head>
<body>
        <!-- Home default Form -->
        <form id="form1" runat="server">
        
            <!-- --- Global head -->
            <header class="global-header">
        
                
        
                <!-- -- navigator -->
                <nav class="headNav">
                    <ul>
                        <li> <a href="index.aspx"> Home </a> </li>
                        <li> <a href="cardapio.aspx"> Cardápio </a> </li>
                        <li> <a href="pedir.aspx"> Peça Aqui <img src="_img/minifood.png" alt="ifood"> </a> </li>
                        <li> <a href="contato.aspx"> Contato </a> </li>
                    </ul>
                </nav>
        
            </header>
        
            <!-- -- logo -->
            <figure class="headLogo"> <img src="_img/logo.png" alt="Pizzaria & Esfirraria Aldebrã"> </figure>
        
            <!-- --- Hero 1st SEC -->
            <div class="heroWrapper">
        
                <section class="heroSec">
        
                    <div class="gradientWprr"></div>
        
                    <div class="heroTitle">
                        <h2> Não há o que questionar  </h2>
                        <h1> Simplesmente a melhor de todo o Vale 🍕 </h1>
                        <h3 class="ligeJa"> <strong> <i class="fas fa-phone"></i> Ligue e peça já! </strong> <span> (12) 3152-8192 </span> </h3>
                    </div>
                    
                    
                    <div class="heropatter"></div>
                </section>  
        
            </div>
        
            <!-- Cardapio -->
        
            <div class="paddingWrapper">
        
                <div class="simpleWrapper">
                    <h2 class="especialidades"> 🍕 Cardápio de Pizzas da casa 🍽 </h2>
                </div>
        
                <br>
            
                <section class="featureCards table-section">
            
                    <!-- table wrappeer -->
                    <div class="tablle-wrapper">

                        <!-- table head -->
                        <div class="table-line header-line">

                            <div class="table-cell"> <strong> Foto </strong> </div>
                            <div class="table-cell"> <strong> Sabor </strong> </div>
                            <div class="table-cell"> <strong> Ingredientes </strong> </div>
                            <div class="table-cell"> <strong> Preço </strong> </div>

                        </div>

                        <!-- lista de sabores -->

                        <!-- baiacatu -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/baiacatu.jpg');">foto <div class="side-pattern"> sp </div> </figure>  </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Baiacatu </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Calabreza </li>
                                    <li> Catupiry </li>
                                    <li> Azeitona </li>
                                    <li> Cebola </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- 4 queijos -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/4quejois.jpg');">foto <div class="side-pattern"> sp </div></figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Quatro Queijos </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho </li>
                                    <li> Mussarela </li>
                                    <li> Catupiry </li>
                                    <li> Parmesão </li>
                                    <li> Provolone </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- Paulista -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/paulista.jpg');">foto <div class="side-pattern"> sp </div></figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Paulista </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list">
                                    <li> Molho de Tomate </li>
                                    <li> Presunto </li>
                                    <li> Palmito </li>
                                    <li> Ovos </li>
                                    <li> Mussarela </li>
                                    <li> Cebola </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- Calabresa -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/calabresa.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Calabresa </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Cebola </li>
                                    <li> Calabresa </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- Brocolis -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/brocolis.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Brócolis </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Brócolis </li>
                                    <li> Calabresa </li>
                                    <li> Alho </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- Alho -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/alho.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Alho </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Alho Frito </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- damaria -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/damaria.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Da Maria </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Catupiry </li>
                                    <li> Calabresa </li>
                                    <li> Frango Desfiado </li>
                                    <li> Bacon </li>
                                    <li> Calabresa </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- Marguerita -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/Marguerita.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Marguerita </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Manjericão </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- portuguesa -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/portuguesa.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Portuguesa </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-10">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Presunto </li>
                                    <li> Ovos </li>
                                    <li> Azeitonas</li>
                                    <li> Cebola </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- Presunto -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/presunto.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Presunto </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Presunto </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- do chefe -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/dochefe.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Do Chefe </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-10">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Presunto </li>
                                    <li> Ovos </li>
                                    <li> Azeitonas</li>
                                    <li> Cebola </li>
                                    <li> Bacon </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- Mussarela -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/Mussarela.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Mussarela </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- palmito -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/Palmito.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Palmito </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-20">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Palmito </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- Mussarela -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/Napolitana.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Napolitana </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-10">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Tomate </li>
                                    <li> Parmesão </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                        <!-- Super peperone -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/Peperone.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span> Pepperoni </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-10">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Pepperoni </li>
                                    <li> Cebola </li>
                                    <li> Azeitonas </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 20,00 </strong> </div> </div>

                        </div>

                        <!-- catupiry -->
                        <div class="table-line content-line">

                            <div class="table-cell"> <figure class="sabor-foto" style="background: url('_img/catupiry.jpg');">foto <div class="side-pattern"> sp </div> </figure> </div>

                            <div class="table-cell sabor-cell"> <div class="flex-cell-wrapper"> <span>  Frango catupiry </span> </div> </div>

                            <div class="table-cell">
                                <ul class="sabores-list margin-ajust-10">
                                    <li> Molho de Tomate </li>
                                    <li> Mussarela </li>
                                    <li> Frango Desfiado </li>
                                    <li> Catupiry </li>
                                </ul>
                            </div>

                            <div class="table-cell price-cell"> <div class="flex-cell-wrapper"> <strong> R$ 16,00 </strong> </div> </div>

                        </div>

                    </div>

                </section>
        
            </div>

            <!-- Cardapio de esfihas -->
        
            <div class="paddingWrapper">
        
                <div class="simpleWrapper">
                    <h2 class="especialidades"> 🥮 Cardápio de Esfihas da casa 🍽 </h2>
                </div>
        
                <br>
            
                <section class="featureCards table-section">
            
                   <div class="esfihas-wrapper">

                        <!-- carne -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/carne.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Carne </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- queijo -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/Queijo.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Queijo </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- calabresa -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/esiCalabresa.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Calabresa </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- atum -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/Atum.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Atum </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- 2 queijos -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/doisqueijos.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> 2 Queijos </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- 3 queijos -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/tresqueijos.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> 3 Queijos </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- frango catupiry -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/frangocatupiry.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Frango Catupiry </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- bacon -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/bacon.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Bacon </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- milho -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/milho.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Milho </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- napolitana -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/esfinapolitana.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Napolitana </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- paulista -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/esfipresunto.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Presunto </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- chocloate -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/chocollate.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Chocolate </h3>
                            <strong> R$ 2,00 </strong>
                        </div>

                        <!-- confeitos -->
                        <div class="esfiha-card">
                            <figure class="esfiha-thumb" style="background: url('_img/chocolate.jpg');"> thumb <div class="cardPattern"> c </div> </figure>
                            <h3> Chocolate com Avelã </h3>
                            <strong> R$ 2,00 </strong>
                        </div>



                   </div>

                </section>
        
            </div>

            <!-- Pandemia -->
        
            <div class="edgeborder edgbrd-white"> -- </div>
            <section class="middleAdvise">
        
                <h6> ⚠️ Recadinho Importante  </h6>
        
                <p>
                    Devido à situação atual de isomento social causado pelo Covid-19, não estamos
                    recebendo clientes para  jantarem na Pizzaria. Nosso atendimento está restrito à entrega e 
                    retirada da refeição direto na porta em frente à pizzaria.
                </p>
        
            </section>
        
            <div class="edgeborder edgbrd-red"> -- </div>
            <footer class="defaultFooter">
        
                <!-- autoria -->
                <div class="autoria">
                    <h3> Pizzaria & Esfiharia Aldebarã - 2020 <small> Todos os Direitos Reservados </small> </h3>
                    <h4> Desenvolvido por Hyper Jobs </h4>
                </div>
        
                <!-- Redes sociais -->
                <div class="redes-sociais">
                    <h2> Acompanhe-nos nas Redes sociais! 👉👉 </h2>
        
                    <nav class="social-icons">
                        <a href="https://www.facebook.com/pizzariaaldebara/" target="_blank"> <i class="fab fa-facebook"></i> </a>
                    </nav>
        
                </div>
        
            </footer>
        
            </form>
</body>
</html>
