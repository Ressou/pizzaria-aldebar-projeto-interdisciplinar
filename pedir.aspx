﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pedir.aspx.cs" Inherits="pedir" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    <!-- Links -->
    <link rel="stylesheet" href="_css/global.css">
    <link rel="stylesheet" href="_fonts/fa/css/all.min.css">

    <!-- metas -->

    <title> Pizzaria Aldebarã | Pedir </title>

</head>
<body>
    <form id="form1" runat="server">

        <!-- --- Global head -->
        <header class="global-header">

            <!-- -- navigator -->
            <nav class="headNav">
                <ul>
                    <li> <a href="index.aspx"> Home </a> </li>
                    <li> <a href="cardapio.aspx"> Cardápio </a> </li>
                    <li> <a href="pedir.aspx"> Peça Aqui <img src="_img/minifood.png" alt="ifood"> </a> </li>
                    <li> <a href="contato.aspx"> Contato </a> </li>
                </ul>
            </nav>

        </header>

    <!-- -- logo -->
    <figure class="headLogo"> <img src="_img/logo.png" alt="Pizzaria & Esfirraria Aldebrã"> </figure>

    <!-- --- Hero 1st SEC -->
    <div class="heroWrapper">

        <section class="heroSec">

            <div class="gradientWprr"></div>

            <div class="heroTitle">
                <h2> Não há o que questionar  </h2>
                <h1> Simplesmente a melhor de todo o Vale 🍕 </h1>
                <h3 class="ligeJa"> <strong> <i class="fas fa-phone"></i> Ligue e peça já! </strong> <span> (12) 3152-8192 </span> </h3>
            </div>
            
            
            <div class="heropatter"></div>
        </section>

    </div>

    <!-- iFood Sect -->
    <div class="iFoodWrapper">

        <div class="ifood-line">

            <!-- - attract -->
            <div class="attract">
                <h3>Estamos no iFood!</h3>
                <p> Baixe o app do iFood na </p>
                
                <figure class="stores">
                    <img src="_img/stores.png" alt="appstore googleplay">
                </figure>

            </div>

            <!-- - ifood icon -->
            <a href="https://www.ifood.com.br/" target="_blank" class="iffodWrapper"> <figure class="ifoodIcon"> <img src="_img/ifood.png" alt="iFood"> </figure> </a>

        </div>

    </div>

    <!-- contato title -->
    <div class="titleWrapper">
        <h2>  🏃‍♀️ Pedido Rápido 🚚 </h2>
        <h3> Se estiver próximo de nós, preencha o formulário abaixo e retire seu pedido no balcão   </h3>
    </div>

    <div class="titleWrapper notaFiscalTitle">
        <h6> Uma vez realizado o pedido, sua nota fiscal será exibida abaixo do formulário: </h6>
    </div>

    <asp:Literal runat="server" ID="ltl"></asp:Literal>

    <!-- Formulário -->
    <div class="formWrapper">

        <div class="form-field">
            
            <label class="inputLabel">
                <span> Para sabermos quem irá retirar: </span>
                <asp:TextBox ID="txtNome" runat="server" CssClass="deftIpt" placeholder="Seu Nome"></asp:TextBox>
            </label>

            <label class="inputLabel">
                <span> Para Evitarmos o uso de Robôs: </span>
                <asp:TextBox ID="txtCpf" runat="server" CssClass="deftIpt" placeholder="Seu CPF"></asp:TextBox>
            </label>

            <label class="inputLabel">
                <span> Para Sabermos o que você deseja (Pizza/Esfiha + Sabor): </span>
                <asp:TextBox ID="txtSabor" runat="server" CssClass="deftIpt" placeholder="Pizza de Mussarela.."></asp:TextBox>
            </label>

            <label class="inputLabel">
                <span> Para Separamos o Troco: </span>
                <asp:TextBox ID="txtTroco" runat="server" CssClass="deftIpt" type="number" placeholder="Troco Para R$.."></asp:TextBox>
            </label>

            <label class="inputLabel">
                <span> Para Preparamos na hora certa: </span>
                <asp:TextBox ID="txtBusca" runat="server" CssClass="deftIpt" placeholder="Que horas você vem buscar?"></asp:TextBox>
            </label>

            <label class="inputLabel bntLabel">
                <asp:Button ID="enviar" runat="server" Text="Realizar o Pedido" CssClass="submitPedido" OnClick="enviar_Click" />
            </label>

        </div>

    </div>



    <!-- nota fiscal Modelo -->
    <!--
    <div class="notaFiscalWrapper">

        <div class="fiscalTop">-</div>

        <div class="notaFiscal">
            <h5> Nota Fiscal </h5>
            <p> <strong> Nome: </strong> <span> José da silva </span> <strong> CPF: </strong> <span> 123.456.456.98 </span> </p>
            <p> <strong> Pedido: </strong> <span> Pizza de Mussarela </span> </p>
            <p> <strong> Troco para: </strong> <span> 50 </span> </p>
            <p> <strong> Senha: </strong> <span> 72382 </span>  </p>
            <p> <strong> Busca: </strong> <span> 18:30 </span> </p>
        </div>

        <div class="fiscalTop bottomFiscal">-</div>

    </div>
    -->
        

            <!-- Pandemia -->
        
            <div class="edgeborder edgbrd-white"> -- </div>
            <section class="middleAdvise">
        
                <h6> ⚠️ Recadinho Importante  </h6>
        
                <p>
                    Devido à situação atual de isomento social causado pelo Covid-19, não estamos
                    recebendo clientes para  jantarem na Pizzaria. Nosso atendimento está restrito à entrega e 
                    retirada da refeição direto na porta em frente à pizzaria.
                </p>
        
            </section>
        
            <div class="edgeborder edgbrd-red"> -- </div>
            <footer class="defaultFooter">
        
                <!-- autoria -->
                <div class="autoria">
                    <h3> Pizzaria & Esfiharia Aldebarã - 2020 <small> Todos os Direitos Reservados </small> </h3>
                    <h4> Desenvolvido por Hyper Jobs </h4>
                </div>
        
                <!-- Redes sociais -->
                <div class="redes-sociais">
                    <h2> Acompanhe-nos nas Redes sociais! 👉👉 </h2>
        
                    <nav class="social-icons">
                        <a href="https://www.facebook.com/pizzariaaldebara/" target="_blank"> <i class="fab fa-facebook"></i> </a>
                    </nav>
        
                </div>
        
            </footer>

    </form>
</body>
</html>
