﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contato.aspx.cs" Inherits="contato" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="icon" type="image/png" href="_img/icon.png" />
    
    <!-- Links -->
    <link rel="stylesheet" href="_css/global.css">
    <link rel="stylesheet" href="_fonts/fa/css/all.min.css">

    <!-- metas -->
     
    <title> Pizzaria Aldebarã | Contato </title>

</head>
<body>
    <form id="form1" runat="server">
        
        <!-- --- Global head -->
        <header class="global-header">

            <!-- -- navigator -->
            <nav class="headNav">
                <ul>
                    <li> <a href="index.aspx"> Home </a> </li>
                    <li> <a href="cardapio.aspx"> Cardápio </a> </li>
                    <li> <a href="pedir.aspx"> Peça Aqui <img src="_img/minifood.png" alt="ifood"> </a> </li>
                    <li> <a href="contato.aspx"> Contato </a> </li>
                </ul>
            </nav>

        </header>

    <!-- -- logo -->
    <figure class="headLogo"> <img src="_img/logo.png" alt="Pizzaria & Esfirraria Aldebrã"> </figure>

    <!-- --- Hero 1st SEC -->
    <div class="heroWrapper">

        <section class="heroSec">

            <div class="gradientWprr"></div>

            <div class="heroTitle">
                <h2> Não há o que questionar  </h2>
                <h1> Simplesmente a melhor de todo o Vale 🍕 </h1>
                <h3 class="ligeJa"> <strong> <i class="fas fa-phone"></i> Ligue e peça já! </strong> <span> (12) 3152-8192 </span> </h3>
            </div>
            
            
            <div class="heropatter"></div>
        </section>

    </div>

    <!-- contato title -->
    <div class="titleWrapper">
        <h2> Estamos em três cidades da região </h2>
        <h3> entre em contato conosco através do número de sua cidade </h3>
    </div>

    <!-- seção de contato -->
    <div class="contatoWrapper">

        <!-- - ribbon -->
        <div class="contatoRibb">

            <!-- - Lorena -->
            <div class="contact-card">

                <figure class="cidade-brasao"> <img src="_img/lorenacab.png" alt="Lorena-sp"> </figure>
                <h4> Lorena-sp </h4>

                <!-- address -->
                <div class="contact-address">

                    <address> Av. Targino Vilela Nunes, 929 - Vila Nunes, Lorena - SP, 12605-530 </address> <hr class="middle-divid">
                    <span class="telefone"> <i class="fas fa-phone"></i> (12) 3152-8192 </span>
                    
                    <a href="" target="https://web.whatsapp.com/_blank" class="zapButton"> <i class="fab fa-whatsapp"></i> <span> Contatar com whatsapp </span> </a>

                </div>

            </div>

            <!-- - Guara -->
            <div class="contact-card">

                <figure class="cidade-brasao"> <img src="_img/guaracab.png" alt="Lorena-sp"> </figure>
                <h4> Guaratinguetá-sp </h4>

                <!-- address -->
                <div class="contact-address">

                    <address> Praça Cristóvão Colombo, 91 - Nova Guará, Guaratinguetá - SP, 12515-470 </address>
                    <address> Av. Nossa Sra. de Fátima, 75 - Vila Santa Rita, Guaratinguetá - SP, 12520-010  </address> <hr class="middle-divid">
                    <span class="telefone"> <i class="fas fa-phone"></i> (12) 3132-3112 </span>
                    
                    <a href="" target="https://web.whatsapp.com/_blank" class="zapButton"> <i class="fab fa-whatsapp"></i> <span> Contatar com whatsapp </span> </a>

                </div>

            </div>

            <!-- - Aparecida     -->
            <div class="contact-card">

                <figure class="cidade-brasao"> <img src="_img/aparecidacab.gif" alt="Lorena-sp"> </figure>
                <h4> Aparecida-sp </h4>

                <!-- address -->
                <div class="contact-address">

                    <address> Av. Antônio Samahá, 124 - Centro, Aparecida - SP, 12570-000 </address> <hr class="middle-divid">
                    <span class="telefone"> <i class="fas fa-phone"></i> (12) 3152-8192 </span>
                    
                    <a href="" target="https://web.whatsapp.com/_blank" class="zapButton"> <i class="fab fa-whatsapp"></i> <span> Contatar com whatsapp </span> </a>

                </div>

            </div>



        </div>

    </div>

            <!-- Pandemia -->
        
            <div class="edgeborder edgbrd-white"> -- </div>
            <section class="middleAdvise">
        
                <h6> ⚠️ Recadinho Importante  </h6>
        
                <p>
                    Devido à situação atual de isomento social causado pelo Covid-19, não estamos
                    recebendo clientes para  jantarem na Pizzaria. Nosso atendimento está restrito à entrega e 
                    retirada da refeição direto na porta em frente à pizzaria.
                </p>
        
            </section>
        
            <div class="edgeborder edgbrd-red"> -- </div>
            <footer class="defaultFooter">
        
                <!-- autoria -->
                <div class="autoria">
                    <h3> Pizzaria & Esfiharia Aldebarã - 2020 <small> Todos os Direitos Reservados </small> </h3>
                    <h4> Desenvolvido por Hyper Jobs </h4>
                </div>
        
                <!-- Redes sociais -->
                <div class="redes-sociais">
                    <h2> Acompanhe-nos nas Redes sociais! 👉👉 </h2>
        
                    <nav class="social-icons">
                        <a href="https://www.facebook.com/pizzariaaldebara/" target="_blank"> <i class="fab fa-facebook"></i> </a>
                    </nav>
        
                </div>
        
            </footer>
        
    </form>
</body>
</html>
