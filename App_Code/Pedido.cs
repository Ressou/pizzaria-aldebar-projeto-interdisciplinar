﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Pedido
/// </summary>
public class PedidoDefault
{
    private string nome;
    private string cpf;

    private string pedido;
    private int troco;
    private int senha;

    private string busca;

    public string Nome { get => nome; set => nome = value; }
    public string Cpf { get => cpf; set => cpf = value; }
    public string Pedido { get => pedido; set => pedido = value; }
    public int Troco { get => troco; set => troco = value; }
    public int Senha { get => senha; set => senha = value; }
    public string Busca { get => busca; set => busca = value; }

    public string Imprimir()
    {
        string str = "";
        str += "<div class='titleWrapper notaFiscalTitle'>";
        str += "<h6> Pedido Realizado!: </h6>";
        str += "</div>";
        str += "<div class='notaFiscalWrapper'>";
        str += "<div class='fiscalTop'>-</div>";
        str += "<div class='notaFiscal'>";
        str += "<h5> Nota Fiscal </h5>";

        str += "<p> <strong> Nome: </strong> <span>" + Nome + "</span> <strong> CPF: </strong> <span> " + Cpf + "</span> </p>";
        str += "<p> <strong> Pedido: </strong> <span>" + Pedido + "</span> </p>";
        str += "<p> <strong> Troco para: </strong> <span>" + Troco + "</span> </p>";
        str += "<p> <strong> Senha: </strong> <span>" + Senha + "</span>  </p>";
        str += "<p> <strong> Busca: </strong> <span>" + Busca + " </span> </p>";

        str += "</div>";
        str += "<div class='fiscalTop bottomFiscal'>-</div>";
        str += "</div>";

        return str;

    }

}
