﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="icon" type="image/png" href="_img/icon.png" />

    <!-- Links -->
    <link rel="stylesheet" href="_css/global.css">
    <link rel="stylesheet" href="_fonts/fa/css/all.min.css">

    <!-- metas -->

    <title> Pizzaria Aldebarã | Home Page </title>

</head>

<body>
    <!-- Home default Form -->
    <form id="form1" runat="server">
        
    <!-- --- Global head -->
    <header class="global-header">

        

        <!-- -- navigator -->
        <nav class="headNav">
            <ul>
                <li> <a href="index.aspx"> Home </a> </li>
                <li> <a href="cardapio.aspx"> Cardápio </a> </li>
                <li> <a href="pedir.aspx"> Peça Aqui <img src="_img/minifood.png" alt="ifood"> </a> </li>
                <li> <a href="contato.aspx"> Contato </a> </li>
            </ul>
        </nav>

    </header>

    <!-- -- logo -->
    <figure class="headLogo"> <img src="_img/logo.png" alt="Pizzaria & Esfirraria Aldebrã"> </figure>

    <!-- --- Hero 1st SEC -->
    <div class="heroWrapper">

        <section class="heroSec">

            <div class="gradientWprr"></div>

            <div class="heroTitle">
                <h2> Não há o que questionar  </h2>
                <h1> Simplesmente a melhor de todo o Vale 🍕 </h1>
                <h3 class="ligeJa"> <strong> <i class="fas fa-phone"></i> Ligue e peça já! </strong> <span> (12) 3152-8192 </span> </h3>
            </div>
            
            
            <div class="heropatter"></div>
        </section>

    </div>

    <!-- Attract Cards -->

    <div class="paddingWrapper">

        <div class="simpleWrapper">
            <h2 class="especialidades"> 👩‍🍳 Especialidades da casa 👨‍🍳 </h2>
        </div>

        <br>
    
        <section class="featureCards">
    
            <!-- espec. Card -->
            <div class="featureCard">
    
                <figure class="cardThumb" style="background: url('_img/damaria.jpg');">
                    <div class="cardPattern"> c </div>
                    
                </figure>
    
                <h3> <span> Da maria </span> <hr> </h3>

                <ul class="cardIngredints">
                    <li> Catupiry  </li> 
                    <li> Mussarela  </li> 
                    <li> Frango  </li> 
                    <li> Calabresa  </li>  
                    <li> Bacon </li> 
                </ul>

                <div class="bottomPrice">
                    <span> R$ 21,99 </span>
                    <a href="" class="anchorPedido"> Pedir agora! </a>
                </div>
                
            </div>

            <!-- espec. Card -->
            <div class="featureCard">
    
                <figure class="cardThumb" style="background: url('_img/baiacatu.jpg');">
                    <div class="cardPattern"> c </div>
                    
                </figure>
    
                <h3> <span> Baiacatu </span> <hr> </h3>

                <ul class="cardIngredints">
                    <li> Molho de tomate  </li> 
                    <li> Mussarela  </li> 
                    <li> Catupiry  </li> 
                    <li> Calabresa  </li>  
                    <li> Azeitona </li> 
                </ul>

                <div class="bottomPrice">
                    <span> R$ 19,99 </span>
                    <a href="" class="anchorPedido"> Pedir agora! </a>
                </div>
                
            </div>

            <!-- espec. Card -->
            <div class="featureCard">
    
                <figure class="cardThumb" style="background: url('_img/dochefe.jpg');">
                    <div class="cardPattern"> c </div>
                    
                </figure>
    
                <h3> <span> Do Chefe </span> <hr> </h3>

                <ul class="cardIngredints">
                    <li> Molho de tomate  </li> 
                    <li> Mussarela  </li> 
                    <li> Peperone  </li> 
                    <li> Cebola  </li>  
                    <li> Azeitonas </li> 
                </ul>

                <div class="bottomPrice">
                    <span> R$ 22,59 </span>
                    <a href="" class="anchorPedido"> Pedir agora! </a>
                </div>
                
            </div>

            <!-- espec. Card -->
            <div class="featureCard">
    
                <figure class="cardThumb" style="background: url('_img/chocolate.jpg');">
                    <div class="cardPattern"> c </div>
                    
                </figure>
    
                <h3> <span> Esfihas Doce </span> <hr> </h3>

                <ul class="cardIngredints">
                    <li> Chocolate  </li> 
                    <li> Avelã  </li> 
                    <li> Confeitos  </li> 
                </ul>

                <div class="bottomPrice">
                    <span> R$ 1,30 <small class="cada"> cada </small> </span>
                    <a href="" class="anchorPedido"> Pedir agora! </a>
                </div>
                
            </div>
    
        </section>

        <!-- enddereços -->
        <div class="betweenWrapper visitenos">
            <h2 class="especialidades"> Venha nos Visitar! </h2>
            <h4 class="descontoTitle"> Aproveite  descontos pegando direto no Balcão !!</h4>
        </div>

    </div>

    <!-- address -->
    <section class="addressLine">

        <figure class="addssFotos thumb"> adr <div class="adrrssPattrn"> adres </div> </figure>

        <!-- lista de cidades -->
        <ul class="citiesList">

            <li> <i class="fas fa-map-marker-alt"></i>
                 <div class="cityMeta">
                     <h5> Lorena - SP </h5>
                     <p>
                        <address> <a href="https://goo.gl/maps/phxZNeCcTPuPFQhS8" class="address-link" target="_blank"> Av. Targino Vilela Nunes, 929 - Vila Nunes, Lorena - SP, 12605-530    <i class="fas fa-location-arrow"></i> </a> </address>
                        <span class="funcionaBadge"> <strong> Funcionamento: </strong> <small> A partir da 18h </small> </span>
                     </p>
                 </div>
            </li>

            <li> <i class="fas fa-map-marker-alt"></i>
                <div class="cityMeta">
                    <h5> Aparecida - SP </h5>
                    <p>
                       <address> <a href="https://goo.gl/maps/buL84Ld49bbqNxkZA" class="address-link" target="_blank"> Av. Antônio Samahá, 124 - Centro, Aparecida - SP, 12570-000 <i class="fas fa-location-arrow"></i> </a> </address>
                       <span class="funcionaBadge"> <strong> Funcionamento: </strong> <small> A partir da 18:30h </small> </span>
                    </p>
                </div>
           </li>

           <li> <i class="fas fa-map-marker-alt"></i>
            <div class="cityMeta">
                <h5> Guaratinguetá - SP </h5>
                <p>
                   <address> <a href="https://goo.gl/maps/FbHWSPsoLEiJhwKcA" class="address-link" target="_blank"> Av. Nossa Sra. de Fátima, 75 - Vila Santa Rita, Guaratinguetá - SP, 12520-010 <i class="fas fa-location-arrow"></i> </a> </address>
                   <span class="funcionaBadge"> <strong> Funcionamento: </strong> <small> A partir da 18:30h </small> </span>
                </p>
            </div>
       </li>

        </ul>

    </section>

    <!-- Pandemia -->

    <div class="edgeborder edgbrd-white"> -- </div>
    <section class="middleAdvise">

        <h6> ⚠️ Recadinho Importante  </h6>

        <p>
            Devido à situação atual de isomento social causado pelo Covid-19, não estamos
            recebendo clientes para  jantarem na Pizzaria. Nosso atendimento está restrito à entrega e 
            retirada da refeição direto na porta em frente à pizzaria.
        </p>

    </section>

    <div class="edgeborder edgbrd-red"> -- </div>
    <footer class="defaultFooter">

        <!-- autoria -->
        <div class="autoria">
            <h3> Pizzaria & Esfiharia Aldebarã - 2020 <small> Todos os Direitos Reservados </small> </h3>
            <h4> Desenvolvido por Hyper Jobs </h4>
        </div>

        <!-- Redes sociais -->
        <div class="redes-sociais">
            <h2> Acompanhe-nos nas Redes sociais! 👉👉 </h2>

            <nav class="social-icons">
                <a href="https://www.facebook.com/pizzariaaldebara/" target="_blank"> <i class="fab fa-facebook"></i> </a>
            </nav>

        </div>

    </footer>

    </form>

</body>
</html>
